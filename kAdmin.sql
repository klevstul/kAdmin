-- -------------------------------------------------------
-- set variables
-- -------------------------------------------------------
set define off





-- .......................................................
-- changelog
-- .......................................................
--
-- v20060815	Frode Klevstul
--		Fixed date field error on data update
--
-- v20060811	Frode Klevstul
--		Support for all PK datatypes on update
--		Fixed error that occured when PK <> an int or number
--
-- v20060425	Frode Klevstul
--		Support for different name format on sequences
--		Support for access control
--		FK links from insert/update form
--
-- v20030921	Frode Klevstul
--		First version made (expansion of a FreePowder.com
--		admin tool also made by Frode Klevstul in 2000)
--
-- .......................................................





-- -------------------------------------------------------
-- prompt information
-- -------------------------------------------------------
PROMPT   + ---------------------------------------------- +
PROMPT   | package: kAdmin                                |
PROMPT   | desc:    A web based DB admin tool in PL/SQL   |
PROMPT   | author:  Frode Klevstul (frode@klevstul.com)   |
PROMPT   | started: 21.09.2003                            |
PROMPT   | version: v20060815                             |
PROMPT   + ---------------------------------------------- +





-- -------------------------------------------------------
-- package header
-- -------------------------------------------------------
CREATE OR REPLACE PACKAGE kAdmin IS

	-- ---------------------------
	-- global declarations
	-- ---------------------------
	type columns_arr 		is table of varchar2(6000) index by binary_integer;
	empty_array 			columns_arr;

	-- ---------------------------
	-- global parameters
	-- ---------------------------
	c_version				constant varchar2(16) 		:= 'v20060815';
	c_sequence_format		constant varchar2(16) 		:= 'SEQ_[TABLENAME]';
	c_debug					boolean						:= false;

	-- ---------------------------
	-- packages and procedures
	-- ---------------------------
	procedure run;
	procedure frames;
	procedure start_page;
	procedure html_start;
	procedure html_stop;
	procedure menu;
	procedure list_packages;
	procedure desc_package
		(
			p_name			in varchar2			default 'kAdmin'
		);
	procedure list_tables;
	procedure list_columns
		(
			p_table_name	in varchar2			default null
		);
	procedure list_sequences;
	procedure admin_table
		(
			p_table_name	in varchar2				default null,
			p_action		in varchar2				default null,
			p_column_name	in columns_arr			default empty_array,
			p_column_data	in columns_arr			default empty_array,
			p_sql_stmt		in varchar2				default null
		);
	procedure debug
		(
			p_msg			in varchar2				default null
		);
	procedure accesscontrol
	(
		p_level		in number		default 0
	);


END;
/
show errors;





-- -------------------------------------------------------
-- package body
-- -------------------------------------------------------
CREATE OR REPLACE PACKAGE BODY kAdmin IS

---------------------------------------------------------
-- Name:        run
-- What:        first page (startup) of the package
-- Author:      Frode Klevstul
-- Start date:  21.09.2003
---------------------------------------------------------
PROCEDURE run
IS
BEGIN

	frames;

END run;



---------------------------------------------------------
-- Name:        frames
-- What:        sets up the admin module in a HTML frameset
-- Author:      Frode Klevstul
-- Start date:  21.09.2003
---------------------------------------------------------
PROCEDURE frames
IS
BEGIN

	accessControl;

	htp.p('
		<html>
		<head>
			<title>kAdmin - '||c_version||'</title>
		</head>

		<frameset cols="200,*" border="0" frameborder="0">
			<frame name="left" src="kAdmin.menu" scrolling="auto" noresize>
			<frame name="main" src="kAdmin.start_page" scrolling="auto" noresize>
		</frameset>

		</html>
		');

END frames;



---------------------------------------------------------
-- Name:        start_page
-- What:        displays a start page
-- Author:      Frode Klevstul
-- Start date:  21.09.2003
---------------------------------------------------------
PROCEDURE start_page
IS
BEGIN

	html_start;
	htp.p('
		<center>
		<br><br><br><br>
		<h1>kAdmin</h1>
		<i>version: '||c_version||'</i>
		<br><br>
		<h3>A web based DB admin tool in PL/SQL</h3>
		<br><br>
		Developed by Frode Klevstul
		<br>
		<a href="http://klevstul.com" target="_blank">www.klevstul.com</a>
		</center>
	');
	html_stop;

END start_page;



---------------------------------------------------------
-- Name:        html_start
-- What:        prints the html code for the top of the page
-- Author:      Frode Klevstul
-- Start date:  22.09.2003
---------------------------------------------------------
PROCEDURE html_start
IS
BEGIN

	htp.p('
		<html>
		<head>
			<title>kAdmin - '||c_version||'</title>

			<STYLE TYPE="text/css">
			<!--
			body,p,td,th,h1,h2,h3,ul,pre {
				font-family : Verdana, Helvetica;
				font-size : 10px;
			}

			textarea,input {
				font-family : Verdana, Helvetica;
				font-size : 12px;
			}

			/* 10px -> 7pt -> xx-small */

			h1 {font-size: 18px}
			h2 {font-size: 14px}
			h3 {font-size: 10px}
			-->
			</STYLE>
		</head>

		<body bgcolor="#ffffff">

	');

END html_start;



---------------------------------------------------------
-- Name:        html_stop
-- What:        prints the html code for the bottom of the page
-- Author:      Frode Klevstul
-- Start date:  22.09.2003
---------------------------------------------------------
PROCEDURE html_stop
IS
BEGIN

	htp.p('
		</font>
		</body>
		</html>
	');

END html_stop;



---------------------------------------------------------
-- Name:        menu
-- What:        prints the overview over all tables
-- Author:      Frode Klevstul
-- Start date:  21.09.2003
---------------------------------------------------------
PROCEDURE menu
IS
BEGIN
DECLARE

	cursor	c_tablenames is
	select	distinct(table_name)
	from	user_tables
	order by table_name asc;

	v_table_name	user_tables.table_name%type		default null;

BEGIN

	accessControl;

	html_start;
	htp.p('
		<font size="1">

		<br><b>DB CONTENT:</b><br>
		<a href="kAdmin.list_packages" target="main">packages</a><br>
		<a href="kAdmin.list_sequences" target="main">sequences</a><br>
		<a href="kAdmin.list_tables" target="main">tables</a><br>

		<br><b>ADMIN TABLES:</b><br>
	');

	open c_tablenames;
	loop
		fetch c_tablenames into v_table_name;
		exit when c_tablenames%NOTFOUND;
		htp.p('<a href="kAdmin.admin_table?p_table_name='||v_table_name||'" target="main">'||v_table_name||'</a></br>');
	end loop;
	close c_tablenames;

	html_stop;
END;
END menu;



---------------------------------------------------------
-- Name:        list_packages
-- What:        lists out all packages for this db user
-- Author:      Frode Klevstul
-- Start date:  21.09.2003
---------------------------------------------------------
PROCEDURE list_packages
IS
BEGIN
DECLARE

	cursor	c_select_name is
	select	distinct(name)
	from	user_source
	where	upper(name) <> 'KADMIN'
	order 	by name ASC;

	v_name	user_source.name%type		default null;

BEGIN

	accessControl;

	html_start;
	htp.p('<table>');
	open c_select_name;
	loop
		fetch c_select_name into v_name;
		exit when c_select_name%NOTFOUND;
		htp.p('<tr><td><a href="kAdmin.desc_package?p_name='||v_name||'">'||v_name||'</a></td></tr>');
	end loop;
	close c_select_name;
	htp.p('</table>');
	html_stop;

END;
END list_packages;



---------------------------------------------------------
-- Name:        desc_package
-- What:        prints the code for a specified package
-- Author:      Frode Klevstul
-- Start date:  21.09.2003
---------------------------------------------------------
PROCEDURE desc_package
	(
		p_name			in varchar2			default 'kAdmin'
	)
IS
BEGIN
DECLARE

	cursor	select_all is
	select	line, text
	from	user_source
	where	name = upper(p_name);

	v_line	user_source.line%type		default null;
	v_text	user_source.text%type		default null;
	v_text2	user_source.text%type		default null;

BEGIN

	accessControl;

	html_start;
	htp.p('<pre>');
	open select_all;
	loop
		fetch select_all into v_line, v_text;
		exit when select_all%NOTFOUND;
		owa_pattern.change(v_text, '<', '\&lt;', 'g');
		owa_pattern.change(v_text, '>', '\&gt;', 'g');
		owa_pattern.change(v_text, '\n', '', 'g');
		htp.p(v_line||':  '||v_text);
	end loop;
	close select_all;
	htp.p('</pre>');
	html_stop;

END;
END desc_package;



---------------------------------------------------------
-- Name:        list_tables
-- What:        lists all tables for this db user
-- Author:      Frode Klevstul
-- Start date:  21.09.2003
---------------------------------------------------------
PROCEDURE list_tables
IS
BEGIN
DECLARE

	cursor	select_all is
	select	distinct(table_name)
	from	user_tables
	order by table_name asc;

	v_table_name	user_tables.table_name%type		default null;

BEGIN

	accessControl;

	html_start;
	htp.p('<table>');
	open select_all;
	loop
		fetch select_all into v_table_name;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td><a href="kAdmin.list_columns?p_table_name='||v_table_name||'">'||v_table_name||'</a></td></tr>');
	end loop;
	close select_all;
	htp.p('</table>');
	html_stop;

END;
END list_tables;



---------------------------------------------------------
-- Name:        list_columns
-- What:        lists all columns for a table
-- Author:      Frode Klevstul
-- Start date:  21.09.2003
---------------------------------------------------------
PROCEDURE list_columns
	(
		p_table_name	in varchar2			default NULL
	)
IS
BEGIN
DECLARE

	cursor	select_all is
	select	column_name, data_type, data_length
	from	user_tab_columns
	where	table_name = p_table_name
	order by column_id asc;

	v_column_name	user_tab_columns.column_name%type		default null;
	v_data_type		user_tab_columns.data_type%type			default null;
	v_data_length	user_tab_columns.data_length%type		default null;

BEGIN

	accessControl;

	html_start;
	htp.p('<table>');
	open select_all;
	loop
		fetch select_all into v_column_name, v_data_type, v_data_length;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td>'||v_column_name||'</td><td>&nbsp;&nbsp;&nbsp;'||v_data_type||'('||v_data_length||')</td></tr>');
	end loop;
	close select_all;
	htp.p('</table>');
	html_stop;

END;
END list_columns;



---------------------------------------------------------
-- Name:        list_sequences
-- What:        lists out all sequences for this db user
-- Author:      Frode Klevstul
-- Start date:  21.09.2003
---------------------------------------------------------
PROCEDURE list_sequences
IS
BEGIN
DECLARE

	cursor	select_all is
	select	sequence_name, min_value, increment_by, last_number
	from	user_sequences
	order by sequence_name asc;

	v_sequence_name	user_sequences.sequence_name%type	default null;
	v_min_value		user_sequences.min_value%type		default null;
	v_max_value		user_sequences.max_value%type		default null;
	v_increment_by	user_sequences.increment_by%type	default null;
	v_last_number	user_sequences.last_number%type		default null;

BEGIN

	accessControl;

	html_start;
	htp.p('<table border="1">');
	htp.p('<tr><td><b>seqence name</b></td><td><b>min value</b></td><td><b>increment by</b></td><td><b>last_number</b></td></tr>');
	open select_all;
	loop
		fetch select_all into v_sequence_name, v_min_value, v_increment_by, v_last_number;
		exit when select_all%NOTFOUND;
		htp.p('<tr><td>'||v_sequence_name||'</td><td>'||v_min_value||'</td><td>'||v_increment_by||'</td><td>'||v_last_number||'</td></tr>');
	end loop;
	close select_all;
	htp.p('</table>');
	html_stop;

END;
END list_sequences;




---------------------------------------------------------
-- Name:        admin_table
-- What:        insert, update, select from a table
-- Author:      Frode Klevstul
-- Start date:  21.09.2003
---------------------------------------------------------
PROCEDURE admin_table
	(
		p_table_name	in varchar2				default NULL,
		p_action		in varchar2				default NULL,
		p_column_name	in columns_arr			default empty_array,
		p_column_data	in columns_arr			default empty_array,
		p_sql_stmt		in varchar2				default NULL
	)
IS
BEGIN
DECLARE

	-- -----------------------------------------------------------------------------
	-- variables assigned to the receiving parameters
	-- -----------------------------------------------------------------------------

	-- defines the action the user chooses, list out, insert, delete, update etc...
	v_action			varchar2(20)							default 'list';

	-- the data/information stored in a column
	v_data				varchar2(4500)							default null;

	-- the sql statement to be executed
	v_sql_stmt			varchar2(4000)							default null;

	-- used as the array where data is received
	v_column_data		columns_arr								:= p_column_data;

	-- -------------------------------------------------------------
	-- used to go through all the columns of the table
	-- selects out the name, the data type and the data length
	-- -------------------------------------------------------------
	cursor	c_column_name is
	select	column_name, data_type, data_length
	from	user_tab_columns
	where	table_name = upper(p_table_name)
	order by column_id asc;

	v_column_name		user_tab_columns.column_name%type		default null;
	v_data_type			user_tab_columns.data_type%type			default null;
	v_data_length		user_tab_columns.data_length%type		default null;

	-- -------------------------------------------------------------
	-- find the name of a constraint in a table
	-- v_constraint_type can be of 'P'=Primary, 'R'=Foreign
	-- selects out the name of the constraint in the table, and the
	-- name of the constraint refered
	-- -------------------------------------------------------------
	cursor	c_constraint_name
	(
		v_table_name		in all_constraints.table_name%type,
		v_constraint_type	in all_constraints.constraint_type%type
	) is
	select	constraint_name, r_constraint_name
	from	all_constraints
	where	table_name = v_table_name
	and		constraint_type = v_constraint_type
	and		status = 'ENABLED';

	v_constraint_name	all_constraints.constraint_name%type	default null;
	v_r_constraint_name	all_constraints.r_constraint_name%type	default null;

	-- ------------------------------------------------------------
	-- find the name of the tabel given a constraint name
	-- ------------------------------------------------------------
	cursor	c_table_name
	(
		v_constraint_name in all_constraints.constraint_name%type
	) is
	select	distinct(table_name)
	from	all_constraints
	where	constraint_name = v_constraint_name;

	v_table_name		all_constraints.r_constraint_name%type	default null;
	v_parent_table		all_constraints.r_constraint_name%type	default null;

	-- ------------------------------------------------------------
	-- given a constraintname, selects out the columns the
	-- constriant applies
	-- made two equal cursors to do a nested lookup
	-- ------------------------------------------------------------
	cursor	c_cname2
	(
		v_constraint_name in all_cons_columns.constraint_name%type
	) is
	select	column_name
	from	all_cons_columns
	where	constraint_name = v_constraint_name;

	cursor	c_cname3
	(
		v_constraint_name in all_cons_columns.constraint_name%type
	) is
	select	column_name
	from	all_cons_columns
	where	constraint_name = v_constraint_name;

	v_column_name_2		all_cons_columns.column_name%type		default null;
	v_parents_pk_column	all_cons_columns.column_name%type		default null;

	-- ------------------------------------------------------------
	-- other variables
	-- ------------------------------------------------------------

	-- help variable to retreive result of "select count(*)" statements
	v_count				number									default null;

	-- this becomes false if there are any errors that prevents the user to submit
	v_submit			boolean									default true;

	-- used to find a describing (name) column in parent table
	v_name_column		all_cons_columns.column_name%type		default null;


	v_array				columns_arr								default empty_array;
	v_pk_array			columns_arr								default empty_array;
	v_pk				number									default null;
	v_i					number									default 0;
	v_name				varchar2(200)							default null;
	v_where_stmt		varchar2(200)							default null;
	v_delete_stmt		varchar2(500)							default null;
	v_delete_stmt_org	varchar2(500)							default null;
	v_pk_in_arr			integer									default null;
	v_text				varchar2(10000)							default null;
	v_tmp				varchar2(50)							default null;
	v_hidden_fields		varchar2(1000)							default null;
	v_foreignkey		boolean									default false;
	v_primarykey		boolean									default false;

	-- dynamical cursor used for SQL statements generated at runtime
	c_dyn_cursor		sys_refcursor;

	-- stores the name of the parent table's constraint
	v_parent_constraint_name	all_constraints.constraint_name%type	default null;

BEGIN

	if (p_action is not null) then
		v_action := p_action;
	end if;

	-- ----------------------
	-- access control check
	-- ----------------------
	if (owa_pattern.match(v_action, '^insert$')) then
		accessControl(1);
	elsif (owa_pattern.match(v_action, '^update$')) then
		accessControl(2);
	elsif (owa_pattern.match(v_action, '^delete$')) then
		accessControl(3);
	else
		accessControl(0);
	end if;

	html_start;
	htp.p('
		<table border="1" width="900">
		<tr><td colspan="3" align="center"><b>'||p_table_name||'</b></td></tr>
		<tr><td colspan="3" align="center">action = '||v_action||'</b></td></tr>
		<tr><td colspan="3">&nbsp;</td></tr>
		<tr><td colspan="3" align="center">
			<a href="kAdmin.admin_table?p_table_name='||p_table_name||'&p_action=list">list</a> |
			<a href="kAdmin.admin_table?p_table_name='||p_table_name||'&p_action=insert">insert</a>
		</td></tr>
	');

	-- ----------------------------------------
	--
	-- insert/update: writes out insert form
	--
	-- ----------------------------------------
	if( owa_pattern.match(v_action, '^insert$') or owa_pattern.match(v_action, '^update$') ) then

		v_column_data := p_column_data;

		htp.p('
			<form action="kAdmin.admin_table" method="post">
			<input type="hidden" name="p_action" value="'||v_action||'_action">
			<input type="hidden" name="p_table_name" value="'||p_table_name||'">
		');
		open c_column_name;
		loop
			fetch c_column_name into v_column_name, v_data_type, v_data_length;
			exit when c_column_name%NOTFOUND;

			debug('*** Column name: '||v_column_name||' ***');
			v_foreignkey := false;
			v_primarykey := false;

			-- --------------------------------
			-- 1: check if it's a primary key
			-- --------------------------------
			open c_constraint_name(p_table_name, 'P');
			loop
				fetch c_constraint_name into v_constraint_name, v_r_constraint_name;
				exit when c_constraint_name%NOTFOUND;

				open c_cname2(v_constraint_name);
				loop
					fetch c_cname2 into v_column_name_2;
					exit when c_cname2%NOTFOUND;

					if (v_column_name = v_column_name_2) then
						v_primarykey := true;
						debug('PRIMARY KEY!');
					end if;

				end loop;
				close c_cname2;

			end loop;
			close c_constraint_name;

			-- --------------------------------
			-- 2: check if it's a foreign key
			-- --------------------------------
			open c_constraint_name(p_table_name, 'R');
			loop
				fetch c_constraint_name into v_constraint_name, v_r_constraint_name;
				exit when c_constraint_name%NOTFOUND;

				open c_cname2(v_constraint_name);
				loop
					fetch c_cname2 into v_column_name_2;
					exit when c_cname2%NOTFOUND;

					if (v_column_name = v_column_name_2) then

						open c_cname3(v_r_constraint_name);
						loop
							fetch c_cname3 into v_column_name_2;
							exit when c_cname3%NOTFOUND;

							v_foreignkey := true;
							v_parent_constraint_name := v_r_constraint_name;
							v_parents_pk_column := v_column_name_2;
							debug('FOREIGN KEY! (' || v_column_name || '&lt;--joins--&gt;' ||v_column_name_2||')');

						end loop;
						close c_cname3;

					end if;

				end loop;
				close c_cname2;

			end loop;
			close c_constraint_name;

			-- ------------------------
			-- forreign key
			-- ------------------------
			if (v_foreignkey) then

				htp.p('
					<input type="hidden" name="p_column_name" value="'||v_column_name||'">
					<tr>
					<td>'||v_column_name||':</td>
				');

				-- selects out the name of the foreign key's parent table
				open c_table_name(v_parent_constraint_name);
				loop
					fetch c_table_name into v_table_name;
					exit when c_table_name%NOTFOUND;
				end loop;
				close c_table_name;

				v_parent_table := v_table_name;

				debug('FK''s parent table: '||v_parent_table);

				-- checks if parent tabel has any entries
				execute immediate 'select count(*) from '||v_parent_table into v_count;

				htp.p('<td>');

				if ( v_count=0 ) then
					htp.p('<font color="#ff0000">error: parent table '''||v_parent_table||''' is empty</font>');
					v_submit := false;
				else
						-- checks if parent table has any columns named "%name%", if it has that information
						-- is displayed together with the primary key
						select	count(*)
						into	v_count
						from	user_tab_columns
						where	table_name = upper(v_parent_table)
						and		column_name like '%NAME%';

						if (v_count>0) then
							select	max(column_name)
							into	v_name_column
							from	user_tab_columns
							where	table_name = upper(v_parent_table)
							and		column_name like '%NAME%';

							v_sql_stmt := 'select '||v_parents_pk_column||','||v_name_column||' from '||v_parent_table;
						else
							v_sql_stmt := 'select '||v_parents_pk_column||' from '||v_parent_table;
						end if;

						-- goes through the parenttabel's entries, and prints out a "select box"
						htp.p('
							<select name="p_column_data">
							<option value="">NULL</option>
						');
						open c_dyn_cursor for v_sql_stmt;
						loop
							if (v_count = 0) then
								fetch c_dyn_cursor into v_array(1);
							else
								fetch c_dyn_cursor into v_array(1), v_array(2);
							end if;
							exit when c_dyn_cursor%NOTFOUND;

							-- if we are updating, we have to display the selected value by default
							if (p_action = 'update') then
								-- checks if the value of the primary key is equal to this columns data,
								-- if it is we mark the field in the select box as "selected"
								if (v_array(1) = v_column_data(c_column_name%ROWCOUNT)) then
									-- store the value that is the FK (and parent's PK, hopefully)
									v_tmp := v_array(1);

									-- checks if this column is a primary key, if it is we passes with the name
									-- of the column, and sets the value to '[PRIMARY_KEY:...]', this is done
									-- so the update procedure can build the right sql statement (with right
									-- WHERE code: ex "... WHERE pk_column = value_of_primary_key")
									if ( v_primarykey ) then
										htp.p('<option value="'||v_array(1)||'" selected>');
										-- passes with the name of the column, and the selected value
										v_hidden_fields := v_hidden_fields ||'
											<input type="hidden" name="p_column_name" value="'||v_column_name||'">
											<input type="hidden" name="p_column_data" value="[PRIMARY_KEY:'||v_array(1)||']">
										';
									else
										htp.p('<option value="'||v_array(1)||'" selected>');
									end if;
								-- not selected
								else
									htp.p('<option value="'||v_array(1)||'">');
								end if;
							-- insert action, nothing is selected from before
							else
								htp.p('<option value="'||v_array(1)||'">');
							end if;

							-- display the value of the primary key, and the value of the '%NAME%'
							-- column (if there is such a column)
							for i in v_array.first..v_array.last loop
								htp.p(v_array(i)||': ');
							end loop;

							htp.p( '</option>' );

						end loop;
						close c_dyn_cursor;
						htp.p( '</select>' );
						
						htp.p('<a href="kAdmin.admin_table?p_action=list&p_table_name='||v_parent_table||'&p_sql_stmt=WHERE%20'||v_parents_pk_column||'='||v_tmp||'" target="_blank">open_FK='||v_tmp||'</a>');
						
				end if;

				htp.p('</td><td>'||v_data_type||'('||v_data_length||')</td></tr>');

			-- -------------------------------
			-- primary key
			-- -------------------------------
			elsif (v_primarykey) then
				htp.p('<input type="hidden" name="p_column_name" value="'||v_column_name||'">');

				-- -------
				-- update
				-- -------
				if (p_action = 'update') then
					htp.p('
						<tr>
							<td>'||v_column_name||':</td>
							<td>'||v_column_data(c_column_name%ROWCOUNT)||'</td>
					');

					-- we have to add extra ' around PK's that are not of type int or number
					if not ( owa_pattern.match(v_data_type, 'number', 'i') or owa_pattern.match(v_data_type, 'int', 'i') ) then
						htp.p('<input type="hidden" name="p_column_data" value="[PRIMARY_KEY:'''||v_column_data(c_column_name%ROWCOUNT)||''']">');					
					else
						htp.p('<input type="hidden" name="p_column_data" value="[PRIMARY_KEY:'||v_column_data(c_column_name%ROWCOUNT)||']">');
					end if;

				-- -------
				-- insert
				-- -------
				elsif (p_action = 'insert') then

					-- -----
					-- datatype has to be of type number or int when inserting a PK, if not we stop the process
					-- -----
					if not ( owa_pattern.match(v_data_type, 'number', 'i') or owa_pattern.match(v_data_type, 'int', 'i') ) then
						htp.p('<tr><td><font color="red">Unsupported PK type</font></td><td><font color="red">'||v_data_type||' not supported as PK</font></td></tr>');
						v_submit := false;

					-- -----
					-- datatype is OK
					-- -----
					else
						-- check if there is a sequence for this primary key, on the format 'c_sequence_format'
						v_tmp := c_sequence_format;
						v_tmp := replace(v_tmp, '[TABLENAME]', upper(p_table_name));
		
						select	count(*)
						into	v_count
						from	user_sequences
						where	sequence_name = v_tmp;
	
						-- we have a sequence with the right name, if we have a update we'll just
						-- display the value of the pk, if it's a insert we'll do a lookup in the
						-- sequence (sequence.nextval)
						if (v_count = 1) then
							htp.p('
								<tr>
									<td>'||v_column_name||':</td>
									<td>'||v_tmp||'.nextval</td>
									<input type="hidden" name="p_column_data" value="[PRIMARY_KEY-SEQ]">
									<td>'||v_data_type||'('||v_data_length||')</td>
								</tr>
							');
						-- there is no sequence for the primary column in this table
						else
							htp.p('
								<tr>
									<td>'||v_column_name||':</td>
									<td>
										<font color="#ff0000">'||v_tmp||' missing</font>
										<a href="kAdmin.admin_table?p_action=create_sequence&p_table_name='||p_table_name||'&p_sql_stmt=CREATE SEQUENCE '||v_tmp||' INCREMENT BY 1 START WITH 1">create sequence SQL</a>
									</td>
									<td>'||v_data_type||'('||v_data_length||')</td>
								</tr>
							');
							v_submit := false;
						end if;
					end if;
				end if;

			-- ------------------------
			-- varchar2
			-- ------------------------
			elsif ( owa_pattern.match(v_data_type, 'varchar2', 'i') ) then

				htp.p('<input type="hidden" name="p_column_name" value="'||v_column_name||'">');

				if ( v_data_length > 200 ) then
					htp.p('
						<tr>
							<td valign="top">'||v_column_name||':</td>
							<td>
								<textarea name="p_column_data" cols="50" rows="10" wrap="off">');
					if (p_action = 'update') then
						v_text := v_column_data(c_column_name%ROWCOUNT);
						owa_pattern.change(v_text, '<', '\&lt;', 'g');
						owa_pattern.change(v_text, '>', '\&gt;', 'g');
						owa_pattern.change(v_text, '"', '\&quot;', 'g');
						htp.p( v_text ||'</textarea>');
					else
						htp.p('</textarea>');
					end if;
					htp.p('
							</td>
							<td valign="top">'||v_data_type||'('||v_data_length||')</td>
						</tr>
					');
				else
					htp.p('
						<tr>
							<td>'||v_column_name||':</td>
							<td>
					');
					if (p_action = 'update') then
						htp.p('<input type="text" name="p_column_data" size="50" maxlength="'||v_data_length||'" value="'||v_column_data(c_column_name%ROWCOUNT)||'">');
					else
						htp.p('<input type="text" name="p_column_data" size="50" maxlength="'||v_data_length||'">');
					end if;
					htp.p('
							</td>
							<td>'||v_data_type||'('||v_data_length||')</td>
						</tr>
					');
				end if;

			-- ------------------------
			-- date
			-- ------------------------
			elsif ( owa_pattern.match(v_data_type, 'date', 'i') ) then

				htp.p('
					<input type="hidden" name="p_column_name" value="'||v_column_name||'">
					<tr>
						<td>'||v_column_name||':</td>
						<td>
				');
				if (p_action = 'update') then
					htp.p('<input type="text" name="p_column_data" size="15" maxlength="14" value="'||to_char(to_date(v_column_data(c_column_name%ROWCOUNT)),'DD/MM YY HH24:MI')||'">');
				else
					htp.p('<input type="text" name="p_column_data" size="15" maxlength="14" value="'||to_char(SYSDATE,'DD/MM YY HH24:MI')||'">');
				end if;
				htp.p('
						</td>
						<td>'||v_data_type||'('||v_data_length||')</td>
					</tr>
				');

			-- ------------------------
			-- number
			-- ------------------------
			elsif ( owa_pattern.match(v_data_type, 'number', 'i') ) then

				htp.p('
					<input type="hidden" name="p_column_name" value="'||v_column_name||'">
					<tr>
						<td>'||v_column_name||':</td>
						<td>
				');
				if (p_action = 'update') then
					htp.p('<input type="text" name="p_column_data" size="50" maxlength="'||v_data_length||'" value="'||v_column_data(c_column_name%ROWCOUNT)||'">');
				else
					htp.p('<input type="text" name="p_column_data" size="50" maxlength="'||v_data_length||'">');
				end if;
				htp.p('
						</td>
						<td>'||v_data_type||'('||v_data_length||')</td>
					</tr>
				');

			else
				htp.p('<tr><td><font color="#ff0000">error: datatype not supported</font> '||v_column_name||'</td><td>&nbsp;&nbsp;&nbsp;'||v_data_type||'('||v_data_length||')</td><td>'||v_data_type||'('||v_data_length||')</td></tr>');
				v_submit := FALSE;
			end if;

		end loop;
		close c_column_name;

		htp.p( v_hidden_fields );		-- writes out hidden-fields from the pk_fk columns (columns that are both fk's and pk's)
		htp.p('
			<tr><td colspan="3">&nbsp;</td></tr>
			<tr><td align="right" colspan="3">
		');

		if (v_submit) then
			htp.p('<input type="submit" value="submit">');
		else
			htp.p('<font color="#ff0000">error</font>');
		end if;

	-- ----------------------------------------
	--
	-- insert_action: insert into the database
	--
	-- ----------------------------------------
	elsif( owa_pattern.match(v_action, '^insert_action$') ) then

		-- selects out and stores all the columns datatypes in an array
		open c_column_name;
		loop
			fetch c_column_name into v_column_name, v_data_type, v_data_length;
			exit when c_column_name%NOTFOUND;
			v_array(c_column_name%ROWCOUNT) := v_data_type;
		end loop;
		close c_column_name;

		-- builds the SQL INSERT statement
		v_sql_stmt := 'INSERT INTO '||p_table_name||' VALUES(';
		for i in p_column_data.first..p_column_data.last loop

			-- stores the data in a variable to be abel to edit the content
			v_data := p_column_data(i);

			-- if primary key has a sequence
			if ( owa_pattern.match(p_column_data(i),'\[PRIMARY\_KEY\-SEQ\]') ) then
				v_tmp := c_sequence_format;
				v_tmp := replace(v_tmp, '[TABLENAME]', upper(p_table_name));

				execute immediate 'SELECT '||v_tmp||'.nextval FROM dual' into v_pk;
				v_sql_stmt := v_sql_stmt ||''||v_pk||',';
			else
				if ( owa_pattern.match(v_array(i), 'VARCHAR2') ) then
					owa_pattern.change(v_data, '''', '''''');		-- substitutes ' with ''
					v_sql_stmt := v_sql_stmt ||''''|| v_data ||''',';
				elsif ( owa_pattern.match(v_array(i), 'NUMBER') ) then
					owa_pattern.change(v_data, ',', '\.');			-- substitutes ',' with '.'
					if ( p_column_data(i) is null) then
						v_sql_stmt := v_sql_stmt ||'NULL,';
					else
						v_sql_stmt := v_sql_stmt ||''|| v_data ||',';
					end if;
				elsif ( owa_pattern.match(v_array(i), 'DATE') ) then
					v_name := p_column_data(i);
					owa_pattern.change(v_name, '\/', '');
					owa_pattern.change(v_name, ':', '');
					owa_pattern.change(v_name, ' ', '', 'g');
					if ( p_column_data(i) is null) then
						v_sql_stmt := v_sql_stmt ||'NULL,';
					else
						v_sql_stmt := v_sql_stmt ||' to_date('''|| v_name ||''',''DDMMYYHH24MI''),';
					end if;
				else
					raise_application_error(-20000, 'Datatype not supported in insert procedure');
				end if;
			end if;

		end loop;
		owa_pattern.change(v_sql_stmt, ',$', ')');

		execute immediate v_sql_stmt;

		owa_pattern.change(v_sql_stmt,'<','\&#060;', 'g');
		owa_pattern.change(v_sql_stmt,'>','\&#062;', 'g');
		owa_pattern.change(v_sql_stmt, '"', '\&quot;', 'g');

		htp.p('<tr><td>'||v_sql_stmt||'</td></tr>');

	-- ----------------------------------------
	--
	-- list: lists out all entries
	--
	-- ----------------------------------------
	elsif( owa_pattern.match(v_action, '^list$')) then

		open c_column_name;
		loop
			fetch c_column_name into v_column_name, v_data_type, v_data_length;
			exit when c_column_name%NOTFOUND;
			v_array(c_column_name%ROWCOUNT) := v_column_name;
		end loop;
		close c_column_name;

		htp.p('<tr><td><table border="1" width="100%">');

		-- builds a select and a delete statement
		htp.p('<tr>');
		v_delete_stmt_org := 'DELETE FROM '||p_table_name||' WHERE';
		v_sql_stmt := 'SELECT ';
		v_i := 1;	-- initializes helpcounter to be 1
		for i in v_array.first..v_array.last loop
			htp.p('<td><a href="kAdmin.admin_table?p_action=list&p_table_name='||p_table_name||'&p_sql_stmt=ORDER BY '||v_array(i)||'"><b>'||v_array(i)||'</b></td>');
			v_sql_stmt := v_sql_stmt ||''||v_array(i)||',';

			-- --------------------------------
			-- check if it's a primary key
			-- --------------------------------
			v_primarykey := false;
			open c_constraint_name(p_table_name, 'P');
			loop
				fetch c_constraint_name into v_constraint_name, v_r_constraint_name;
				exit when c_constraint_name%NOTFOUND;
				open c_cname2(v_constraint_name);
				loop
					fetch c_cname2 into v_column_name_2;
					exit when c_cname2%NOTFOUND;
					if (v_array(i) = v_column_name_2) then
						v_primarykey := true;
					end if;
				end loop;
				close c_cname2;
			end loop;
			close c_constraint_name;

			if ( v_primarykey ) then
				v_delete_stmt_org := v_delete_stmt_org ||' '||v_array(i)||'���:'||v_i||' AND';
				v_pk_array(v_i) := i;	-- stores where in the array you'll find the pk
				v_i := v_i + 1;
			end if;
		end loop;

		-- changes the last ',' with the rest of the sql code, the "p_sql_stmt" contains "order by" code
		owa_pattern.change(v_sql_stmt, ',$', ' FROM '||p_table_name||' '||p_sql_stmt);
		-- removes the last AND, since there'll always be one too many
		owa_pattern.change(v_delete_stmt_org, 'AND$', '');

		htp.p('<td>&nbsp;</td><td>&nbsp;</td></tr>');

		-- finds out how many entries there are in the array
		v_i := v_array.last;

		open c_dyn_cursor for v_sql_stmt;
		loop
			-- There SHOULD be a better way to do this, but I've not found
			-- out how, this sucks!!
			if (v_i = 1) then
				fetch c_dyn_cursor into v_array(1);
			elsif (v_i = 2) then
				fetch c_dyn_cursor into v_array(1),v_array(2);
			elsif (v_i = 3) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3);
			elsif (v_i = 4) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4);
			elsif (v_i = 5) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5);
			elsif (v_i = 6) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6);
			elsif (v_i = 7) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7);
			elsif (v_i = 8) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8);
			elsif (v_i = 9) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9);
			elsif (v_i = 10) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10);
			elsif (v_i = 11) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11);
			elsif (v_i = 12) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12);
			elsif (v_i = 13) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13);
			elsif (v_i = 14) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14);
			elsif (v_i = 15) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15);
			elsif (v_i = 16) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16);
			elsif (v_i = 17) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17);
			elsif (v_i = 18) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17),v_array(18);
			elsif (v_i = 19) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17),v_array(18),v_array(19);
			elsif (v_i = 20) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17),v_array(18),v_array(19),v_array(20);
			elsif (v_i = 21) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17),v_array(18),v_array(19),v_array(20),v_array(21);
			elsif (v_i = 22) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17),v_array(18),v_array(19),v_array(20),v_array(21),v_array(22);
			elsif (v_i = 23) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17),v_array(18),v_array(19),v_array(20),v_array(21),v_array(22),v_array(23);
			elsif (v_i = 24) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17),v_array(18),v_array(19),v_array(20),v_array(21),v_array(22),v_array(23),v_array(24);
			elsif (v_i = 25) then
				fetch c_dyn_cursor into v_array(1),v_array(2),v_array(3),v_array(4),v_array(5),v_array(6),v_array(7),v_array(8),v_array(9),v_array(10),v_array(11),v_array(12),v_array(13),v_array(14),v_array(15),v_array(16),v_array(17),v_array(18),v_array(19),v_array(20),v_array(21),v_array(22),v_array(23),v_array(24),v_array(25);
			else
				raise_application_error(-20000, 'more colums selected than fetched');
			end if;

			exit when c_dyn_cursor%NOTFOUND;

			htp.p('
				<tr>
				<form action="kAdmin.admin_table" method="post">
			');

			for i in v_array.first..v_array.last loop
				owa_pattern.change(v_array(i),'<','\&#060;', 'g');
				owa_pattern.change(v_array(i),'>','\&#062;', 'g');
				owa_pattern.change(v_array(i), '"', '\&quot;', 'g');
				htp.p( v_text );
				htp.p('<td valign="top">'||v_array(i)||'<input type="hidden" name="p_column_data" value="'||v_array(i)||'"></td>');
			end loop;

			-- goes through the DELETE statement and replaces ':1', ':2' etc
			-- with the right values
			v_delete_stmt := v_delete_stmt_org;
			debug(v_delete_stmt);
			for i in v_pk_array.first..v_pk_array.last loop
				owa_pattern.change(v_delete_stmt, ':'||i , v_array(v_pk_array(i)));
			end loop;

			htp.p( '
				<td valign="top">
					<nobr>
					<input type="hidden" name="p_table_name" value="'||p_table_name||'">
					<input type="hidden" name="p_action" value="update">
					<input type="submit" value="update">
					</form>
					</nobr>
				</td>
				<td valign="top">
					<a href="kAdmin.admin_table?p_action=delete&p_table_name='||p_table_name||'&p_sql_stmt='||v_delete_stmt||'">delete</a>
				</td>
			');

		end loop;
		close c_dyn_cursor;

	-- ----------------------------------------
	--
	-- delete: confirm deleting an entry
	--
	-- ----------------------------------------
	elsif( owa_pattern.match(v_action, '^delete$')) then

		v_sql_stmt := p_sql_stmt;
		owa_pattern.change(v_sql_stmt,'���','=', 'g');

		htp.p('<tr><td>'||v_sql_stmt||'?</td></tr>');

		owa_pattern.change(v_sql_stmt,'DELETE','SELECT *','i');
		htp.p('
			<tr><td><br>Are you sure you want run this delete statement?<br>You will delete ALL data referenced to this database entry (CASCADE delete).<br><br></td></tr>
			<tr><td>
				<a href="javascript:history.back(-1)">don''t delete</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a href="kAdmin.admin_table?p_action=delete_action&p_table_name='||p_table_name||'&p_sql_stmt='||p_sql_stmt||'">delete</a>
			</td></tr>
			<tr><td><br>If you''re not 100% sure, run this SQL statement to see what you are deleting:<br><i>'||v_sql_stmt||';</i><br><br></td></tr>
		');

	-- ----------------------------------------
	--
	-- delete: delete an entry
	--
	-- ----------------------------------------
	elsif( owa_pattern.match(v_action, '^delete_action$')) then

		v_sql_stmt := p_sql_stmt;
		owa_pattern.change(v_sql_stmt,'���','=', 'g');

		htp.p('<tr><td>'||v_sql_stmt||'</td></tr>');

		execute immediate v_sql_stmt;

	-- ----------------------------------------
	--
	-- create_sequence: SQL to create an sequence
	--
	-- ----------------------------------------
	elsif( owa_pattern.match(v_action, '^create_sequence$')) then

		htp.p('<tr><td><i>'|| p_sql_stmt ||';</i></td></tr>');

	-- ----------------------------------------
	-- update_action: updates entry
	-- ----------------------------------------
	elsif( owa_pattern.match(v_action, '^update_action$')) then

		-- select out all columns datatypes and stores this info in an array
		open c_column_name;
		loop
			fetch c_column_name into v_column_name, v_data_type, v_data_length;
			exit when c_column_name%NOTFOUND;
			v_array(c_column_name%ROWCOUNT) := v_data_type;
		end loop;
		close c_column_name;

		v_sql_stmt := 'UPDATE '||p_table_name||' SET ';

		for i in p_column_data.first..p_column_data.last loop
			v_data := p_column_data(i);

			if ( owa_pattern.match(p_column_data(i),'\[PRIMARY\_KEY:') ) then
				v_name := p_column_data(i);
				owa_pattern.change(v_name, '\[PRIMARY\_KEY:', '');
				owa_pattern.change(v_name, '\]', '');
				v_where_stmt := v_where_stmt ||' '|| p_column_name(i) ||'='|| v_name ||' AND';
			else
				if ( owa_pattern.match(v_array(i), 'VARCHAR2') ) then
					owa_pattern.change(v_data, '''', '''''');	-- change ' with ''
					v_sql_stmt := v_sql_stmt ||' '||p_column_name(i)||'='''|| v_data ||''',';
				elsif ( owa_pattern.match(v_array(i), 'NUMBER') ) then
					owa_pattern.change(v_data, ',', '\.');	-- change ',' with '.'
					if ( p_column_data(i) is null) then
						v_sql_stmt := v_sql_stmt ||' '||p_column_name(i)||'=NULL,';
					else
						v_sql_stmt := v_sql_stmt ||' '||p_column_name(i)||'='|| v_data ||',';
					end if;
				elsif ( owa_pattern.match(v_array(i), 'DATE') ) then
					owa_pattern.change(v_data, '\/', '');
					owa_pattern.change(v_data, ':', '');
					owa_pattern.change(v_data, ' ', '', 'g');
					if ( p_column_data(i) is null) then
						v_sql_stmt := v_sql_stmt ||' '||p_column_name(i)||'=NULL,';
					else
						v_sql_stmt := v_sql_stmt ||' '||p_column_name(i)||'=to_date('''|| v_data ||''',''DDMMYYHH24MI''),';
					end if;
				end if;
			end if;

		end loop;

		owa_pattern.change(v_where_stmt,'AND$','');
		owa_pattern.change(v_sql_stmt,',$',' WHERE '||v_where_stmt);

		execute immediate v_sql_stmt;

		owa_pattern.change(v_sql_stmt,'<','\&#060;', 'g');
		owa_pattern.change(v_sql_stmt,'>','\&#062;', 'g');
		owa_pattern.change(v_sql_stmt, '"', '\&quot;', 'g');

		htp.p('<tr><td><i>'|| v_sql_stmt ||';</i></td></tr>');

	end if;

	htp.p('</table></td></tr></form></table>');
	html_stop;

	-- ---------------------
	-- exception handeling
	-- ---------------------
	exception
		when no_data_found then
			htp.p('<font color="#ff0000">error: NO_DATA_FOUND exception</font>');

--		when others then
--			raise_application_error(-20000, v_sql_stmt);

END;
END admin_table;



---------------------------------------------------------
-- Name:        debug
-- What:        prints out debugging information - for development
-- Author:      Frode Klevstul
-- Start date:  22.09.2003
---------------------------------------------------------
PROCEDURE debug
	(
		p_msg			in varchar2				default null
	)
IS
BEGIN
DECLARE

	print	boolean 			:= c_debug;

BEGIN

	if (print) then
		htp.p('<font color="red">DEBUG: '||p_msg||'</font><br>');
	end if;

END;
END debug;



---------------------------------------------------------
-- Name:        accessControl
-- What:        access control
-- Author:      Frode Klevstul
-- Start date:  25.04.2006
---------------------------------------------------------
PROCEDURE accessControl
(
	p_level		in number		default 0
)
IS
BEGIN
DECLARE

	v_access	boolean			default false;

BEGIN

	-- ********************************************************************
	-- YOUR ACCESS CONTROL LOGIC HERE
	-- ********************************************************************
	--
	-- Note:
	-- 		the heigher p_level the stricter the rules should be:
	--
	--		list:		level 0
	--		insert:		level 1
	--		update:		level 2
	--		delete:		level 3
	--
	-- ********************************************************************
	ses_lib.ini('kAdmin', 'list', null);
	ses_lib.ini('kAdmin', 'insert', null);
	ses_lib.ini('kAdmin', 'update', null);
	ses_lib.ini('kAdmin', 'delete', null);

	if (p_level = 0) then

		if not (mod_lib.hasAccess('kAdmin', 'list')) then
			v_access := false;
		else
			v_access := true;
		end if;

	elsif (p_level = 1) then

		if not (mod_lib.hasAccess('kAdmin', 'insert')) then
			v_access := false;
		else
			v_access := true;
		end if;

	elsif (p_level = 2) then

		if not (mod_lib.hasAccess('kAdmin', 'update')) then
			v_access := false;
		else
			v_access := true;
		end if;

	elsif (p_level = 3) then

		if not (mod_lib.hasAccess('kAdmin', 'delete')) then
			v_access := false;
		else
			v_access := true;
		end if;

	end if;
	-- ************************************
	-- END OF YOUR LOGIC
	-- ************************************

	-- -----------
	-- stop if no access
	-- -----------
	if (v_access = false) then
		raise_application_error(-20000, 'NO ACCESS');
	end if;

END;
END accessControl;



-- -------------------------------------------------------
-- end package and show errors
-- -------------------------------------------------------
END; -- ends package body
/
show errors;


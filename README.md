kAdmin
======

kAdmin is a PL/SQL package that lets user administer an Oracle database through a web interface.


Note:
This code should be rewritten, to take advantage of ["Method 4"] (http://posterous.klevstul.com/klevstul/posts/2009/12/Method%204-8449563.html), a method that I discovered after I wrote kAdmin.

Example code:
<pre>
---------------------------------------------------------
-- name:        run
-- what:        run report given with name p_name, using method 4
-- author:      Frode Klevstul
-- start date:  07.12.2009
---------------------------------------------------------
procedure run
(
      p_name                    in rep_report.name%type
)
is
    c_procedure                 constant varchar2(32)                   := 'run';

    v_report                    rep_report%rowtype;
    v_sql_select                varchar2(2000);
    v_sql_insert                varchar2(32767);
    v_time1                     number;
    v_time2                     number;
    v_run_time_cs               rep_run.run_time_cs%type;

    v_cursor                    number;
    v_execute                   number;
    v_col_cnt                   pls_integer;
    v_rec_tab                   dbms_sql.desc_tab;
    v_result                    varchar2(1024);
    v_row_count                 int                                     := 0;
    v_sqlerrm                   log_exception.error_message%type;
    v_clob                      clob;
    v_clob_tmp                  clob;
    v_write_pk                  wri_write.pk%type;

begin
    v_time1 := dbms_utility.get_time;

    select  *
    into    v_report
    from    rep_report
    where   lower(name) = lower(p_name);

    -- processor override handling
    -- if we're going to override the processor it means we'll do a direct injection to the write object.
    -- we need a clob to store the content. plus we need to open a new write object with a blank order
    if (v_report.bool_override_processor = 1) then
        ext_lob.ini(v_clob);

        -- add an empty order so the write object will be created
        insert into imp_order
        (template_name)
        values (v_report.name);

        -- run the processor for the order we just added, for the write object to be created++
        processor.run;

        -- get the pk of the newly created write order
        select  seq_wri_write.currval
        into    v_write_pk
        from    dual;
        commit;

        commit;

    end if;

    if (v_report.bool_inactive = 1) then
        raise_application_error(-20000, 'FK says: "Report <'||p_name||'> is inactive"');
    elsif (v_report.bool_running = 1) then
        raise_application_error(-20000, 'FK says: "Report <'||p_name||'> is already running. Please wait..."');
    end if;

    -- update rep_report to show it is running (cancel any error message)
    update  rep_report
    set     feedback        = null
        ,   bool_running    = 1
        ,   bool_error      = null
    where   pk = v_report.pk;
    commit;

    if (g_debug) then dbms_output.put_line('p_name = '||p_name); end if;

    -- get the first part, until "from", in the sql statement (note that this will fail if the select list contain "from " as well, but it should not...)
    v_sql_select := substr(v_report.sql_statement, 0, instr( lower(v_report.sql_statement), 'from ', 1 , 1 ) );

    -- count the number of items in the select statement - (count number of the character ',' in string v_sql_select and adds 1 to the result)
    select  length(v_sql_select) - length(replace((v_sql_select), ',')) + 1
    into    v_col_cnt
    from    dual;

    -- open, parse and describe the columns we're going to fetch
    v_cursor := dbms_sql.open_cursor;
    dbms_sql.parse(v_cursor, v_report.sql_statement, dbms_sql.native);
    dbms_sql.describe_columns(v_cursor, v_col_cnt, v_rec_tab);

    -- prepare columns to select
    for i in v_rec_tab.first..v_rec_tab.last
    loop
        dbms_sql.define_column(v_cursor, i, v_report.sql_statement, 4000);
    end loop;

    -- execute, fetch and display results
    v_execute := dbms_sql.execute(v_cursor);
    while(dbms_sql.fetch_rows(v_cursor)>0)
    loop
        v_row_count := v_row_count + 1;

        -- build insert statement
        v_sql_insert := 'insert into imp_order (template_name, '||v_report.insert_to_string||') values('''||v_report.name||'''';

        -- loop through all records
        for i in v_rec_tab.first..v_rec_tab.last
        loop
            dbms_sql.column_value(v_cursor, i, v_result);

            -- continue building insert statement
            v_sql_insert := v_sql_insert||', '''||replace(v_result, '''', '''''')||'''';

            -- processor override handling
            if (v_report.bool_override_processor = 1) then
                if (i>1) then
                    -- NOTE: when overriding the processor the delimiter is ALWAYS semicolon ";",
                    -- it is always replaced with an ":" if found in the data and chr(10) is always used for new line
                    v_clob_tmp := v_clob_tmp||';';
                end if;
                v_clob_tmp := v_clob_tmp || replace(v_result, ';', ':');
            end if;

            if (g_debug) then dbms_output.put_line('i = '||i||' - v_result = '||v_result); end if;
        end loop;

        -- finish building the insert statement
        v_sql_insert := v_sql_insert||')';

        -- processor override handling
        if (v_report.bool_override_processor = 1) then
            v_clob_tmp := v_clob_tmp||chr(10);
            ext_lob.add(v_clob, v_clob_tmp);

        else
            -- execute the insert statement
            execute immediate v_sql_insert;
            commit;
        end if;

        if (g_debug) then dbms_output.put_line(v_sql_insert); end if;
    end loop;

    -- close the cursor
    dbms_sql.close_cursor(v_cursor);

    -- processor override handling
    if (v_report.bool_override_processor = 1) then
        update  wri_write
        set     content = content || v_clob
        where   pk = v_write_pk;
        commit;
    end if;

    -- add EOF to template
    if (v_row_count > 0) then
        processor.addEof(v_report.name);
    end if;

    -- update rep_report to show it has finished running
    update  rep_report
    set     bool_running = null
    where   pk = v_report.pk;
    commit;

    v_time2 := dbms_utility.get_time;
    v_run_time_cs := v_time2 - v_time1;

    logRun(v_report.pk, v_run_time_cs, v_row_count);

exception
    when others then
        if (v_report.pk is not null) then
            v_sqlerrm := sqlerrm;
            update  rep_report
            set     feedback        = v_sqlerrm
                ,   bool_running    = null
                ,   bool_error      = 1
            where   pk              = v_report.pk;
            commit;
        end if;
        processor.exceptionHandle(sqlcode, sqlerrm, g_package||'.'||c_procedure);
        null;
end run;
</pre>
